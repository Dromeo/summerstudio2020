# Summer Studio 2020
Hey guys,

Hope you guys have had a happy new year and a good break.

A few things before we kick off our summer studio - we have a little assessment to ensure that you are competent enough to be able to undertake it.

In order to get started you will need one of the following software:
Anaconda or Docker.

Since we will be using Docker a significant amount in this studio - I have made the quiz readily available on a Docker Image. 
For those who are unable to figure out how to set up Docker - you can attempt the quiz by downloading the notebook and numpy array file (.npz file) with an Anaconda Installtion.

If you have any questions - please try Google first. (You will be doing a significant amount of Googling...). 

```Please do not post your solution or code on Microsoft teams or it will be regarded as plagiarism.```

If you still haven't had your questions answered - feel free to post in the Microsoft teams chat. 

Cheers,
Ben

## Docker Installation
### Step 1 - Get Docker Desktop for your computer
Install Docker for your laptop that you plan to bring to class. Check their website on how to install it. 
Make sure you give 'sudo' privileges to Docker for you Linux and Mac Users 
(It's not recommended on the website - but I am too lazy to type in my password every time I want to use Docker. I gurantee you will be too.)

### Step 2 - Getting the Docker Image for the Quiz
Run the below command to get the Docker Image. 

```bash
docker pull benthepleb/summerstudio2020:quiz
```

### Step 3 - Launching the Docker Container
Copy and paste the below command to launch the Docker Container to get the Quiz up and Running

```bash
docker run --rm -it -p 8888:8888 --privileged benthepleb/summerstudio2020:quiz jupyter lab --port 8888 --ip 0.0.0.0 --allow-root --no-browser
```

### Step 4 - Jupyter Lab Quiz Attempt
Make sure you copy and paste the output from the terminal/powershell to open the notebook to attempt the quiz. 
Please read the instructions on the notebook to finalise the quiz. The link should start with 127.0.0.1.

### Step 5 - Read the instructions on the quiz and do thy quiz.