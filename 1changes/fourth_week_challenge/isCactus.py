import cv2
import os
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Flatten, Conv2D, MaxPool2D, Activation, Dense, Dropout
from tensorflow.keras.preprocessing.image import ImageDataGenerator

train_directory = 'train'
test_directory = 'test'

df = pd.read_csv('train.csv')
df.head()

img = cv2.imread('train/0004be2cfeaba1c0361d39e2b000257b.jpg')
plt.imshow(img)
img.shape

train_datagen = ImageDataGenerator(
    rescale=1./255,
    validation_split=0.1,
)

df['has_cactus'] = df['has_cactus'].astype(str)
train_generator = train_datagen.flow_from_dataframe(
    df,
    directory = train_directory,
    subset = 'training',
    x_col = 'id',
    y_col = 'has_cactus',
    target_size = (32,32),
    class_mode = 'binary'
)
val_generator = train_datagen.flow_from_dataframe(
    df,
    directory = train_directory,
    subset = 'validation',
    x_col = 'id',
    y_col = 'has_cactus',
    target_size = (32,32),
    class_mode = 'binary'
)

model = Sequential()
model.add(Conv2D(32, (3,3) ,activation = 'relu', input_shape = (32,32,3)))
model.add(MaxPool2D(2,2))
model.add(Conv2D(64, (3,3), activation='relu'))
model.add(MaxPool2D(2,2))
model.add(Conv2D(128, (3,3), activation='relu'))
model.add(MaxPool2D(2,2))
model.add(Flatten())
model.add(Dense(512, activation = 'relu'))
model.add(Dropout(0.2))
model.add(Dense(1, activation = 'sigmoid'))

model.compile(
    loss = 'binary_crossentropy',
    optimizer = 'rmsprop', 
    metrics = ['accuracy'])

history = model.fit_generator(
    train_generator,
    steps_per_epoch = 1000,
    epochs = 5,
    validation_data = val_generator,
    validation_steps = 32
)

ids = []
X_test = []
for image in os.listdir(test_directory):
    ids.append(image.split('.')[0])
    path = os.path.join(test_directory, image)
    X_test.append(cv2.imread(path))
    
X_test = np.array(X_test)
X_test = X_test.astype('float32') / 255

predictions = model.predict(X_test)

submission = pd.read_csv('sample_submission.csv')
submission['has_cactus'] = predictions
submission['id'] = ids
submission.head(10)
submission.to_csv('submission.csv', index = False)
