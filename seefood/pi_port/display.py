import matplotlib.pyplot as plt

img = plt.imread('code.png')
plt.imshow(img, cmap='gray')
plt.axis('off')
plt.title('Hot Dog Web App Link')
plt.suptitle('Your uploaded images do not get saved anywhere')
plt.show()
